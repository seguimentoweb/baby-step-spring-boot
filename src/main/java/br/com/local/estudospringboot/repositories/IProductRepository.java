package br.com.local.estudospringboot.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

// import org.springframework.data.repository.CrudRepository;

import br.com.local.estudospringboot.entity.Product;

public interface IProductRepository extends PagingAndSortingRepository<Product, Integer> {
    // Alguns nomes de convesoes
    /**
     * findByAttributeModelNameContaining
     * findByAttributeModelNameIsContaining
     * findByNameContainingIgnoreCase
     * findByAttributeModelNameContais
     * findByAttributeModelNameStartsWith
     * findByAttributeModelNameEndWith
     * findByAttributeModelNameNotContaining
     */
    // @Query jpql optinial
    // Tem que seguir a convensão do nome do metodo
    public Iterable<Product> findByNameContaining(String name);
    public Iterable<Product> findByNameContainingIgnoreCase(String name);
    @Query("Select p from Product p where p.name like %:name%")
    public Iterable<Product> findByLikeName(@Param("name") String name);
}
