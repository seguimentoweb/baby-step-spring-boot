package br.com.local.estudospringboot.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/")
public class HomeController {

	//@RequestMapping(method = RequestMethod.GET, path = "/hello")
	@GetMapping(path = "hello")
	public String helloWorld() {
		return "Hello World Spring Boot";
	}


}
