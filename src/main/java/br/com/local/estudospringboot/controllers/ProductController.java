package br.com.local.estudospringboot.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import br.com.local.estudospringboot.entity.Product;
import br.com.local.estudospringboot.repositories.IProductRepository;

@RestController
@RequestMapping(path = "products")
public class ProductController {

    @Autowired
    private IProductRepository productRepository;

    @PostMapping("store")
    public @ResponseBody Product store(@RequestParam String name, @RequestParam Double price,
            @RequestParam Double discount) {
        Product product = new Product(name, price, discount);
        System.out.println(product.repository);
        productRepository.save(product);
        return product;
    }

    @PostMapping("create")
    // @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public @ResponseBody Product create(@Validated Product product) {
        productRepository.save(product);
        return product;
    }

    @GetMapping
    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    @GetMapping(path = "{id}")
    public Optional<Product> findfindById(@PathVariable Integer id) {
        return productRepository.findById(id);
    }

    @PutMapping
    public Product update(@Validated Product product) {
        productRepository.save(product);
        return product;
    }

    @DeleteMapping(path = "{id}")
    public String delete(@PathVariable Integer id) {
        productRepository.deleteById(id);
        return "Prodcut delete success";
    }

    // Paginacao
    /// Ficar ligado nos imports do pageable
    @GetMapping(path = "paginate/{page}/{perPage}")
    public Iterable<Product> paginate(@PathVariable Integer page, @PathVariable Integer perPage) {
        if (perPage > 50) {
            perPage = 50;
        }
        Pageable pageNumber = PageRequest.of(page, perPage);
        return productRepository.findAll(pageNumber);
    }

    @GetMapping(path = "/name/{name}")
    public Iterable<Product> findByNameContaining(String name) {
        return productRepository.findByNameContaining(name);
    }

    @GetMapping(path = "/name/{iname}")
    public Iterable<Product> findByNameContainingIgnoreCase(String iname) {
        return productRepository.findByNameContainingIgnoreCase(iname);
    }
}
