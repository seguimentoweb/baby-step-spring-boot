package br.com.local.estudospringboot.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.local.estudospringboot.models.Client;

@RestController
@RequestMapping(path = "clients")
public class ClientController {

	@GetMapping(path = "store")
	public Client store() {
		return new Client(1, "Alexandre", "1234567890");
	}

	@GetMapping(path = "edit/{id}")
	public Client edit(@PathVariable int id) {
		return new Client(id, "Alexandre", "123456789000");
	}

	@GetMapping(path = "index")
	public Client index(@RequestParam(name = "id", required = true, defaultValue = "null") int id) {
		return new Client(id, "Alexandre", "12345");
	}
}
