package br.com.local.estudospringboot.controllers;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "methods")
public class MethosHttpController {

    @GetMapping(path = "get")
    public String get() {
        return "Eu sou get";
    }

    @PostMapping(path = "post")
    public String post() {
        return "Eu sou post";
    }

    @PutMapping(path = "put")
    public String put() {
        return "Eu sou put";
    }

    @PatchMapping(path = "path")
    public String path() {
        return "Eu sou update";
    }

    @DeleteMapping(path = "delete")
    public String delete() {
        return "Eu sou delete";
    }

    public String options() {
        return "Eu sou options";
    }
}
