package br.com.local.estudospringboot.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
// import javax.validation.constraints.Min;
// import javax.validation.constraints.Max;
// import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.local.estudospringboot.repositories.IProductRepository;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    // @NotBlack
    private String name;
    // @Min(0)
    private Double price;
    // @Min(0)
    // @Max(1)
    private Double discount;
    @Autowired
    @Transient
    public IProductRepository repository;

    public Product() {
    }

    public Product(String name, Double price, Double discount) {
        super();
        this.name = name;
        this.price = price;
        this.discount = discount;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return this.discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }
}
